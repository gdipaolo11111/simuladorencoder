# simuladorEncoder

Sencillo simulador de encoder para lo que no tengan el HARD, se carga en un arduino independiente del que estas testeando y por puerto sere simulas el giro con 4<>6 y el click con 5

Usa los pines

|         |pin|
| ------- | - |
| Señal A | 2 |
| Señal B | 3 |
| Click   | 4 |
