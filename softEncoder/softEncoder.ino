int serie = 0;
//Pines
#define A 2
#define B 3
#define C 4

int state=0;

void setup()
{
  Serial.begin(9600);

  pinMode(A, OUTPUT); //a
  pinMode(B, OUTPUT); //b
  pinMode(C, OUTPUT); //click
}

void loop()
{
  if(Serial.available())
  {
    serie = Serial.read();
    if (serie == '4') {
      Serial.println("-");
      state--;
      if(state<0) state=3;
    }
    if (serie == '6') {
      Serial.println("+");
      state++;
      if(state>3) state=0;
    }
    if (serie == '5') {
      Serial.println("X");
      digitalWrite(C, HIGH);
      delay(500);
      digitalWrite(C, LOW);
    }
    delay(10); // Delay a little bit to improve simulation performance

    switch(state)
    {
      case 0: 
      digitalWrite(A,LOW);
      digitalWrite(B,LOW);
      break;
      case 1: 
      digitalWrite(A,HIGH);
      digitalWrite(B,LOW);
      break;
      case 2: 
      digitalWrite(A,HIGH);
      digitalWrite(B,HIGH);
      break;
      case 3: 
      digitalWrite(A,LOW);
      digitalWrite(B,HIGH);
      break;
    }
  }
}